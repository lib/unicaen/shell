<?php

namespace UnicaenShell;

use UnicaenShell\Command\Example\NoopShellCommand;
use UnicaenShell\Command\ShellCommandAbstractFactory;
use UnicaenShell\Options\ModuleOptions;
use UnicaenShell\Options\ModuleOptionsFactory;

return [
    'unicaen-shell' => [
        'commands' => [
            NoopShellCommand::class => [
                //'executable' => ':', // possibilité de spécifier le chemin de l'exécutable ici
            ],
        ],
    ],

    'service_manager' => [
        'abstract_factories' => [
            ShellCommandAbstractFactory::class,
        ],
        'factories' => [
            ModuleOptions::class => ModuleOptionsFactory::class,
        ],
    ],
];