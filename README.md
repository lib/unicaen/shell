Bibliothèque unicaen/shell
==========================
**Bibliothèque de création et lancement de commandes shell.**

*Si vous cherchez qqchose de plus carré et ambitieux, regardez du côté du composant Symfony 
[Process](https://symfony.com/doc/current/components/process.html).*


Classe de commande 
------------------

```php
use UnicaenShell\Command\ShellCommand;

final class NoopShellCommand extends ShellCommand
{
    protected string $executable = ':'; // no op

    public function getName(): string
    {
        return 'NoopShellCommand';
    }

    public function generateCommandLine()
    {
        $this->commandLine = $this->executable;
    }
}
```

Cf. la classe mère [ShellCommand](src/UnicaenShell/Command/ShellCommand.php). 


Config
------

Exemple :

```php
    'unicaen-shell' => [
        'commands' => [
            \UnicaenShell\Command\Example\NoopShellCommand::class => [
                //'executable' => ':', // écraserait {@see \UnicaenShell\Command\Example\NoopShellCommand::$executable}
            ],
        ],
    ],
```

Cette config est exploitée par la factory abstraite [ShellCommandAbstractFactory](src/UnicaenShell/Command/ShellCommandAbstractFactory.php).


Obtention via le service manager
--------------------------------

```php
/** @var \UnicaenShell\Command\Example\NoopShellCommand $command */
$command = $container->get(\UnicaenShell\Command\Example\NoopShellCommand::class);
```


Lancement classique
-------------------

```php
use UnicaenShell\Command\ShellCommandRunnerTrait;

class MonController extends \Laminas\Mvc\Controller\AbstractActionController
{
    public function oneAction() 
    {
        $result = $this->runShellCommand($this->command);
        // ...      
    }
}
```


Lancement avec temps d'exécution max (timeout)
----------------------------------------------

```php
use UnicaenShell\Command\ShellCommandRunnerTrait;

class MonService
{
    public function genererPdf() 
    {
        try {
            // Un timeout peut être appliqué au lancement de la commande.
            // Si ce timout est atteint, l'exécution de la commande est interrompue
            // et une exception TimedOutCommandException est levée.
            $result = $this->runShellCommand($this->command, '10s');
        } catch (TimedOutCommandException $toce) {
            //...
            // Exemple : lancer une commande en tâche de fond qui fait la même chose mais qui envoie le PDF par mail.
            //... 
        }
    }
}
```


Lancement en tâche de fond
--------------------------

```php
use UnicaenShell\Command\ShellCommandRunnerTrait;

class MonService
{
    public function genererPdf() 
    {
        // Lance une commande en arrière-plan (nohup + &).
        // Du coup, pas de collecte de résultat ici.
        $this->runShellCommandInBackground($this->command);
    }
}
```


Usage explicite
---------------

Il est bien-sûr possible de créer/manipuler directement commande et runner, exemple :

```php
// lancement de la commande de retraitement du fichier PDF en tâche de fond
$destinataires = $newFichierThese->getFichier()->getHistoModificateur()->getEmail();
$command = new RetraitementShellCommand();
$command->setDestinataires($destinataires);
$command->setFichierThese($fichierThese);
$command->generateCommandLine();
$runner = new ShellCommandRunner();
$runner->setCommand($command);
try {
    $runner->runCommandInBackground();
} catch (\UnicaenShell\Command\Exception\ShellCommandException $e) {
    throw new RuntimeException("Erreur survenue lors du lancement de la commande de retraitement", 0, $e);
}
```
