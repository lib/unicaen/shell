<?php

namespace UnicaenShell\Options;

trait ModuleOptionsAwareTrait
{
    protected ModuleOptions $moduleOptions;

    public function setModuleOptions(ModuleOptions $moduleOptions): void
    {
        $this->moduleOptions = $moduleOptions;
    }
}