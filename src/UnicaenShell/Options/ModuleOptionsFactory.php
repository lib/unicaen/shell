<?php

namespace UnicaenShell\Options;

use Psr\Container\ContainerInterface;
use UnicaenShell\Module;
use Webmozart\Assert\Assert;

/**
 * @author Unicaen
 */
class ModuleOptionsFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ModuleOptions
    {
        $config = $container->get('config');

        Assert::keyExists(
            $config,
            'unicaen-shell',
            "La clé %s spécifiant la config du module " . Module::NAME . " est introuvable dans la config de l'application"
        );
        $moduleConfig = $config['unicaen-shell'];

        Assert::keyExists(
            $moduleConfig,
            'commands',
            "La clé %s est introuvable dans la config 'unicaen-shell'"
        );
        $commandsConfig = $moduleConfig['commands'];

        $moduleConfig = new ModuleOptions();
        $moduleConfig->setCommands($commandsConfig);

        return $moduleConfig;
    }
}