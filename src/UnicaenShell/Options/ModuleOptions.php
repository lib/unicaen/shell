<?php

namespace UnicaenShell\Options;

/**
 * Classe encapsulant les options de fonctionnement du module.
 *
 * @author Unicaen
 */
class ModuleOptions
{
    protected array $commands = [];

    public function getCommands(): array
    {
        return $this->commands;
    }

    public function setCommands(array $commands): self
    {
        $this->commands = $commands;
        return $this;
    }
}