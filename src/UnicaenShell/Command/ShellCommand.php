<?php

namespace UnicaenShell\Command;

use UnicaenShell\Command\Exception\ShellCommandException;

abstract class ShellCommand implements ShellCommandInterface
{
    /**
     * @var string Chemin de l'exécutable ou du script à lancer.
     */
    protected string $executable;

    /**
     * @var array
     */
    protected array $options = [];

    protected string $commandLine;

    protected string $outputFilePath;

    /**
     * @var string[]
     */
    protected array $inputFilesPaths;

    protected string $errorFilePath;

    public function setOutputFilePath(string $outputFilePath): self
    {
        $this->outputFilePath = $outputFilePath;
        return $this;
    }

    /**
     * @param string[] $inputFilesPaths
     */
    public function setInputFilesPaths(array $inputFilesPaths): self
    {
        $this->inputFilesPaths = $inputFilesPaths;
        return $this;
    }

    public function setErrorFilePath(string $errorFilePath): self
    {
        $this->errorFilePath = $errorFilePath;
        return $this;
    }

    public function setOptions(array $options): self
    {
        $this->options = array_merge($this->options, $options);

        if (isset($options['executable'])) {
            $this->executable = $options['executable'];
        }

        return $this;
    }

    /**
     * Vérifie l'existence de l'exécutable.
     *
     * @throws ShellCommandException En cas de ressources ou pré-requis manquants
     */
    public function assertExecutableExists()
    {
        exec("which $this->executable", $output, $returnCode);

        if ($returnCode !== 0) {
            throw new ShellCommandException("L'exécutable '$this->executable' de la commande '$this' semble introuvable");
        }
    }

    public function checkRequirements(): void
    {

    }

    public function getCommandLine(): string
    {
        return $this->commandLine;
    }

    public function createResult(array $output, int $returnCode): ShellCommandResult
    {
        return new ShellCommandResult($output, $returnCode);
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}