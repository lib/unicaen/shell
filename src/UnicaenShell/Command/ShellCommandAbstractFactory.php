<?php

namespace UnicaenShell\Command;

use Laminas\ServiceManager\Exception\InvalidArgumentException;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;
use UnicaenShell\Options\ModuleOptions as UnicaenShellModuleOptions;

/**
 * Factory abstraite permettant de construire des {@see \UnicaenShell\Command\ShellCommand} configurées comme suit :
 *
 * ```php
 *'unicaen-shell' => [
 *    'commands' => [
 *        BannerShellCommand::class => [
 *            'executable' => '/usr/bin/banner', // écrasera {@see \UnicaenShell\Command\ShellCommand::$executable}
 *        ],
 *    ],
 *],
 * ```
 */
class ShellCommandAbstractFactory implements AbstractFactoryInterface
{
    protected ?array $commandConfig;

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function canCreate(ContainerInterface $container, $requestedName): bool
    {
        $this->commandConfig = $this->getCommandConfig($container, $requestedName);
        if (!$this->commandConfig) {
            return false;
        }

        return true;
    }

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): ShellCommandInterface
    {
        /** @var \UnicaenShell\Command\ShellCommandInterface $command */
        $command = new $requestedName();
        if (!$command instanceof ShellCommandInterface) {
            throw new InvalidArgumentException("La classe de commande spécifiée doit être de type " . ShellCommandInterface::class);
        }

        $command->setOptions($this->commandConfig);

        return $command;
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function getCommandConfig(ContainerInterface $container, $requestedName): ?array
    {
        /** @var UnicaenShellModuleOptions $unicaenShellModuleOptions */
        $unicaenShellModuleOptions = $container->get(UnicaenShellModuleOptions::class);
        $commands = $unicaenShellModuleOptions->getCommands();

        if (!array_key_exists($requestedName, $commands)) {
            return null;
        }

        return $commands[$requestedName];
    }
}