<?php

namespace UnicaenShell\Command;

interface ShellCommandInterface
{
    /**
     * Retourne le "petit nom" de cette commande.
     */
    public function getName(): string;

    /**
     * Spécifie les options de fonctionnement de cette commande.
     */
    public function setOptions(array $options);

    /**
     * Spécifie le chemin du fichier de sortie.
     */
    public function setOutputFilePath(string $outputFilePath): self;

    /**
     * Génère la ligne de commande à lancer.
     */
    public function generateCommandLine();

    /**
     * Vérification que les prérequis pour lancer la commande sont réunis.
     *
     * @throws \UnicaenShell\Command\Exception\ShellCommandException En cas de problème
     */
    public function checkRequirements();

    /**
     * Retourne la ligne de commande générée si {@see generateCommandLine()} a été appelée auparavant.
     */
    public function getCommandLine(): string;

    /**
     * Créée le résultat de l'exécution de cette commande.
     */
    public function createResult(array $output, int $returnCode): ShellCommandResultInterface;
}