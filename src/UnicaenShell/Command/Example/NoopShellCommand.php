<?php

namespace UnicaenShell\Command\Example;

use UnicaenShell\Command\ShellCommand;

/**
 * Exemple de commande.
 */
final class NoopShellCommand extends ShellCommand
{
    protected string $executable = ':'; // no op

    public function getName(): string
    {
        return 'NoopShellCommand';
    }

    public function generateCommandLine()
    {
        $this->commandLine = $this->executable;
    }
}